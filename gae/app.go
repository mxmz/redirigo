package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	//	"bytes"
	"context"

	"gitlab.com/mxmz/redirigo/responder"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2/jwt"
	"google.golang.org/api/drive/v3"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"

	// "google.golang.org/api/plus/v1"
	"google.golang.org/api/sheets/v4"
	//	"google.golang.org/appengine"
)

var globalRouteMap responder.ResponderMap
var globalRouteMapMutex sync.Mutex
var globalRouteMapRefreshed int64

const ConfigPath = `https://gitlab.com/mxmz/redirigo/raw/master/data/paths`

var responderMapC chan<- responder.ResponderMap

func init() {
	//http.HandleFunc("/", handler)
	//http.HandleFunc("/pino", handlerPino)

}
func main() {
	http.HandleFunc("/", handler)

	appengine.Main()
	// port := os.Getenv("PORT")
	// if port == "" {
	// 	port = "8080"
	// 	log.Printf("Defaulting to port %s", port)
	// }

	// log.Printf("Listening on port %s", port)
	// if err := http.ListenAndServe(":"+port, nil); err != nil {
	// 	log.Fatal(err)
	// }
}

/*
func __getReponderMap(r *http.Request) responder.ResponderMap {
	globalRouteMapMutex.Lock()
	defer globalRouteMapMutex.Unlock()

	if globalRouteMap != nil {
		var now = time.Now().Unix()
		if now-globalRouteMapRefreshed > 60 {
			globalRouteMapRefreshed = now
			globalRouteMap = nil
		}
	}

	if globalRouteMap == nil {

		if globalRouteMap == nil {

			ctx := appengine.NewContext(r)
			client := urlfetch.Client(ctx)
			// glog.Debugf(ctx, "config: %s", ConfigPath)
			resp, err := client.Get(ConfigPath)
			defer resp.Body.Close()
			if err == nil {
				globalRouteMap, _ = responder.NewResponderMapFromConfig(resp.Body, true)
			}
			// glog.Debugf(ctx, "config: %v %v %v", ConfigPath, err, globalRouteMap)

		}
	}
	return globalRouteMap

}
*/
func handler(w http.ResponseWriter, r *http.Request) {
	// ctx := appengine.NewContext(r)
	// fmt.Fprint(w, "Hello, world!")
	// return
	rm := getReponderMap(r)

	path := strings.Split(r.URL.Path, "/")[1:]
	//fmt.Fprint(w, path)
	//log.Printf("handler: %v %v", path, rm)

	route, extra := rm.ResolveRoute(path)

	// glog.Debugf(ctx, "handler: %v", route)

	if route != nil {
		route.(responder.Responder).Respond(w, r, extra)
	} else {
		fmt.Fprint(w, "Hello, world!")
	}

}

// func handlerPino(w http.ResponseWriter, r *http.Request) {
// 	c := appengine.NewContext(r)

// 	b, err := ioutil.ReadFile("key.json")
// 	if err != nil {
// 		panic(err)
// 	}

// 	config, err := google.JWTConfigFromJSON(b, drive.DriveMetadataReadonlyScope, "https://www.googleapis.com/auth/spreadsheets.readonly", "profile")
// 	if err != nil {
// 		panic(err)
// 	}

// 	transport := &oauth2.Transport{
// 		Source: config.TokenSource(c),
// 		Base:   &urlfetch.Transport{Context: c},
// 	}
// 	client := &http.Client{Transport: transport}

// 	// PLUS SERVICE CLIENT
// 	pc, err := plus.New(client)
// 	if err != nil {
// 		// glog.Errorf(c, "An error occurred creating Plus client: %v", err)
// 	}
// 	person, err := pc.People.Get("me").Do()
// 	if err != nil {
// 		// glog.Errorf(c, "Person Error: %v", err)
// 	}
// 	// glog.Infof(c, "Name: %v", person)

// 	// DRIVE CLIENT
// 	dc, err := drive.New(client)
// 	if err != nil {
// 		// glog.Errorf(c, "An error occurred creating Drive client: %v", err)
// 	}

// 	{

// 		r, err := dc.Files.Get("1zMP-feq_RbHrRESyCiXi6wWeBfAgqIK2AKbXCB-ps4E").Do()

// 		if err != nil {
// 			panic(err)
// 		}
// 		fmt.Fprintf(w, "Get: %v", r)

// 		srv, err := sheets.New(client)
// 		if err != nil {
// 			panic(err)
// 		}

// 		readRange := "redirigo!A1:Z16"
// 		resp, err := srv.Spreadsheets.Values.Get("1zMP-feq_RbHrRESyCiXi6wWeBfAgqIK2AKbXCB-ps4E", readRange).Do()
// 		if err != nil {
// 			panic(err)
// 		}
// 		var paths []string

// 		if len(resp.Values) > 0 {
// 			fmt.Fprintln(w, "Name, Major:")
// 			for _, row := range resp.Values {
// 				// Print columns A and E, which correspond to indices 0 and 4.
// 				fmt.Fprintf(w, "%d %v\n", len(row), row)
// 				if len(row) > 0 {
// 					paths = append(paths, row[0].(string))
// 					for _, v := range row[1:] {
// 						paths = append(paths, " ", v.(string))
// 					}
// 					paths = append(paths, "\n")
// 				}
// 			}
// 			var pathsText = strings.Join(paths, "")
// 			fmt.Fprintf(w, "%s\n", pathsText)

// 		} else {
// 			fmt.Fprint(w, "No data found.")
// 		}

// 	}

// 	{
// 		r, err := dc.Files.List().PageSize(10).
// 			Fields("nextPageToken, files(id, name)").Do()
// 		if err != nil {
// 			// glog.Errorf(c, "Unable to retrieve files: %v", err)
// 		}

// 		fmt.Println("Files:")
// 		if len(r.Files) > 0 {
// 			for _, i := range r.Files {
// 				fmt.Fprintf(w, "%s (%s)\n", i.Name, i.Id)
// 			}
// 		} else {
// 			fmt.Fprint(w, "No files found.")
// 		}
// 	}

// 	fmt.Fprint(w, "Hello, Pino!")
// }

type textResponder struct {
	text string
}

func (tr *textResponder) Respond(w http.ResponseWriter, r *http.Request, extra []string) {
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintf(w, tr.text)
}

func getReponderMap(r *http.Request) responder.ResponderMap {
	globalRouteMapMutex.Lock()
	defer globalRouteMapMutex.Unlock()

	if globalRouteMap != nil {
		var now = time.Now().Unix()
		if now-globalRouteMapRefreshed > 60 {
			globalRouteMapRefreshed = now
			globalRouteMap = nil
		}
	}

	if globalRouteMap == nil {

		ctx := r.Context() //   .NewContext(r)
		text, err := loadTextFromSheet(ctx, "1zMP-feq_RbHrRESyCiXi6wWeBfAgqIK2AKbXCB-ps4E")
		if err == nil {
			globalRouteMap, _ = responder.NewResponderMapFromConfig(strings.NewReader(text), func(r *http.Request) *http.Client {
				ctx := appengine.NewContext(r)
				return urlfetch.Client(ctx)
			}, true)
			globalRouteMap.AddRoute([]string{"0792cc92-82a3-47ce-bca9-60724dc99b8b"},
				&textResponder{text})
		} else {
			panic(err)
		}
		// log.Printf("config: %v %v %v\n", text, err, globalRouteMap)

	}
	return globalRouteMap

}

var oauth2Config *jwt.Config

func loadTextFromSheet(c context.Context, sheetId string) (string, error) {

	if oauth2Config == nil {
		b, err := ioutil.ReadFile("gae/key.json")
		if err != nil {
			return "", err
		}
		oauth2Config, err = google.JWTConfigFromJSON(b, drive.DriveMetadataReadonlyScope, sheets.SpreadsheetsReadonlyScope)
		if err != nil {
			return "", err
		}
	}

	var client = oauth2Config.Client(c)

	// glog.Debugf(c, "%v", googleToken)

	// transport := &oauth2.Transport{
	// 	Source: googleToken,
	// 	//Base:   &urlfetch.Transport{Context: c},

	// }
	// client := &http.Client{Transport: transport}

	{
		srv, err := sheets.New(client)
		if err != nil {
			return "", err
		}

		readRange := "redirigo!A1:Z16"
		resp, err := srv.Spreadsheets.Values.Get(sheetId, readRange).Do()
		if err != nil {
			return "", err
		}
		var paths []string
		if len(resp.Values) > 0 {
			for _, row := range resp.Values {
				if len(row) > 0 {
					paths = append(paths, row[0].(string))
					for _, v := range row[1:] {
						paths = append(paths, " ", v.(string))
					}
					paths = append(paths, "\n")
				}
			}
			var pathsText = strings.Join(paths, "")
			return pathsText, nil
		} else {
			return "", nil
		}
	}
	return "", nil
}
