package responder

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/mxmz/redirigo/routemap"
)

type Responder interface {
	Respond(w http.ResponseWriter, r *http.Request, extra []string)
}

type ResponderMap interface {
	ResolveRoute(path []string) (interface{}, []string)
	AddRoute(path []string, v interface{})
}

type routeMap interface {
	ResponderMap
}

type HttpClient func(c *http.Request) *http.Client

func NewResponderMapFromConfig(config io.Reader, c HttpClient, sync bool) (ResponderMap, error) {
	scanner := bufio.NewScanner(config)
	var rmap routeMap
	if sync {
		rmap = routemap.NewRouteMapMT()
	} else {
		rmap = routemap.NewRouteMap()
	}
	for scanner.Scan() {
		t := scanner.Text()

		fields := strings.Fields(t)
		if len(fields) == 0 {
			continue
		}
		if len(fields) > 0 && fields[0][0] == '#' {
			continue
		}
		if len(fields) != 3 {
			panic("bad config line: " + t)
		}

		path := strings.Split(fields[0][1:], "/")
		if fields[1] == `GOIMPORT` {
			rmap.AddRoute(
				path,
				NewGoImportResponder(fields[0][1:], fields[2]),
			)
		} else if fields[1] == `REDIRECT` {
			rmap.AddRoute(
				path,
				NewRedirectResponder(fields[2]),
			)
		} else if fields[1] == `FETCH` {
			rmap.AddRoute(
				path,
				NewFetchResponder(fields[2], c),
			)
		}

	}
	return rmap, nil
}

type GoImportResponder struct {
	goImportPath string
	repoURI      string
}

func NewGoImportResponder(goImportPath string, repoURI string) *GoImportResponder {
	return &GoImportResponder{
		goImportPath: goImportPath,
		repoURI:      repoURI,
	}
}

func (rsp *GoImportResponder) Respond(w http.ResponseWriter, r *http.Request, extra []string) {
	hostname := nvl(r.Header.Get("X-Appengine-Server-Name"), r.Header.Get("Host"))
	fmt.Fprintf(w, `<html><head><meta name="go-import" content="%s git %s" /></head></html>`,
		hostname+"/"+rsp.goImportPath,
		rsp.repoURI,
	)
}

type RedirectResponder struct {
	targetURI string
}

func NewRedirectResponder(targetURI string) *RedirectResponder {
	return &RedirectResponder{
		targetURI: targetURI,
	}
}

func (rsp *RedirectResponder) Respond(w http.ResponseWriter, r *http.Request, extra []string) {
	w.Header().Set("Location", rsp.targetURI)
	w.WriteHeader(302)
}

func nvl(ss ...string) string {
	for _, s := range ss {
		if len(s) > 0 {
			return s
		}
	}
	return ""
}

type FetchResponder struct {
	targetURI string
	c         HttpClient
}

func NewFetchResponder(targetURI string, c HttpClient) *FetchResponder {
	return &FetchResponder{
		targetURI: targetURI,
		c:         c,
	}
}

func (rsp *FetchResponder) Respond(w http.ResponseWriter, r *http.Request, extra []string) {
	w.WriteHeader(200)
	client := rsp.c(r)

	res, err := client.Get(rsp.targetURI)

	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	txt, err := ioutil.ReadAll(res.Body)

	w.Header().Set("Content-Type", "text/plain")
	w.Write(txt)
}
