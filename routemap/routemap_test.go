package routemap

import "testing"
import "strings"
import . "github.com/onsi/gomega"

type TestRouteMap interface {
	ResolveRoute(path []string) (interface{}, []string)
	AddRoute(path []string, v interface{})
}

func TestRouteMap_ResolveRoute(t *testing.T) {
	RegisterTestingT(t)
	rm := NewRouteMap()
	Expect(rm).NotTo(BeNil())
	testResolveRoute(rm)
}

func TestRouteMapMT_ResolveRoute(t *testing.T) {
	RegisterTestingT(t)
	rm := NewRouteMapMT()
	Expect(rm).NotTo(BeNil())
	testResolveRoute(rm)
}

func testResolveRoute(rm TestRouteMap) {

	rm.AddRoute([]string{"prova", "pluto"}, 42)

	{
		v, extra := rm.ResolveRoute([]string{"prova", "pluto"})

		Expect(v).To(BeEquivalentTo(42))
		Expect(extra).To(BeEmpty())

	}

	{
		v, extra := rm.ResolveRoute([]string{"prova", "pluto", "pippo"})

		Expect(v).To(BeEquivalentTo(42))
		Expect(extra).To(BeEquivalentTo([]string{"pippo"}))

	}
	{
		v, extra := rm.ResolveRoute([]string{"prova", "pluto", "pippo", "topolino"})

		Expect(v).To(BeEquivalentTo(42))
		Expect(extra).To(BeEquivalentTo([]string{"pippo", "topolino"}))

	}
	{
		v, extra := rm.ResolveRoute([]string{"missing", "pluto", "pippo", "topolino"})

		Expect(v).To(BeNil())
		Expect(extra).To(BeEquivalentTo([]string{"missing", "pluto", "pippo", "topolino"}))

	}
	rm.AddRoute([]string{"prova", "pluto", "pippo", "topolino"}, 4242)
	{
		v, extra := rm.ResolveRoute([]string{"prova", "pluto", "pippo", "topolino"})

		Expect(v).To(BeEquivalentTo(4242))
		Expect(extra).To(BeEquivalentTo([]string{}))

	}
	rootPath := "/"

	rm.AddRoute(strings.Split(rootPath[1:], "/"), 4243)

	{
		v, extra := rm.ResolveRoute([]string{"missing", "pluto", "pippo", "topolino"})

		Expect(v).To(BeEquivalentTo(4243))
		Expect(extra).To(BeEquivalentTo([]string{"missing", "pluto", "pippo", "topolino"}))

	}

	_ = rm
}
