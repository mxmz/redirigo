package routemap

import (
	"sync"
)

type node struct {
	children map[string]*node
	val      interface{}
}

func newNode() *node {
	return &node{
		children: map[string]*node{},
		val:      nil,
	}
}

type RouteMap struct {
	root *node
}

func NewRouteMap() *RouteMap {

	return &RouteMap{
		root: newNode(),
	}
}

func (rm *RouteMap) AddRoute(path []string, v interface{}) {

	curr := rm.root
	for _, comp := range path {
		if comp == "" {
			continue
		}
		n, ok := curr.children[comp]
		if ok {
			curr = n
		} else {
			n := newNode()
			curr.children[comp] = n
			curr = n
		}
	}
	curr.val = v
}

// ResolveRoute resolves a route
func (rm *RouteMap) ResolveRoute(path []string) (interface{}, []string) {

	curr := rm.root
	for i, comp := range path {
		n, ok := curr.children[comp]
		if !ok {
			return curr.val, path[i:]
		}
		curr = n
	}
	return curr.val, []string{}

}

type RouteMapMT struct {
	m RouteMap
	l sync.Mutex
}

func NewRouteMapMT() *RouteMapMT {
	return &RouteMapMT{
		m: RouteMap{
			root: newNode(),
		},
	}
}

func (rm *RouteMapMT) ResolveRoute(path []string) (interface{}, []string) {
	rm.l.Lock()
	defer rm.l.Unlock()
	return rm.m.ResolveRoute(path)
}

func (rm *RouteMapMT) AddRoute(path []string, v interface{}) {
	rm.l.Lock()
	defer rm.l.Unlock()
	rm.m.AddRoute(path, v)
}
